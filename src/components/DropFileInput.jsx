import React, { useRef, useState } from 'react';
import { ImageConfig } from '../config/ImageConfig';
// import PropTypes from 'prop-types';
import './drop-file-input.css';
import uploadImg from '../../assets/cloud-upload-regular-240.png';

const DropFileInput = ({ onFileChange }) => {
  const wrapperRef = useRef(null);
  const [fileList, setFileList] = useState([]);

  const onDragEnter = () => wrapperRef.current.classList.add('dragover');
  const onDragLeave = () => wrapperRef.current.classList.remove('dragover');
  const onDrop = () => wrapperRef.current.classList.remove('dragover');

  const onFileDrop = (e) => {
    const newFile = e.target.files[0];

    if (newFile) {
      const updateList = [...fileList, newFile];
      setFileList(updateList);
      onFileChange(updateList);
    }
  };

  const fileRemove = (file) => {
    const updateList = [...fileList];
    updateList.splice(fileList.indexOf(file), 1);
    setFileList(updateList);
    onFileChange(updateList);
  };
  return (
    <>
      <div
        ref={wrapperRef}
        onDragEnter={onDragEnter}
        onDragLeave={onDragLeave}
        onDrop={onDrop}
        className='drop-file-input'
      >
        <div className='drop-file-input__label'>
          <img src={uploadImg} alt='' />
          <p>DropFileInput</p>
        </div>
        <input type='file' value='' onChange={onFileDrop} />
      </div>
      {fileList.length > 0 ? (
        <div className='drop-file-preview'>
          <p className='drop-file-preview__title'>Archivos subidos</p>
          {fileList.map(
            (item, index) => (
              console.log(item.type.split('/')[1]),
              (
                <div key={index} className='drop-file-preview__item'>
                  <img
                    src={
                      ImageConfig[item.type.split('/')[1]] ||
                      ImageConfig['default']
                    }
                    alt=''
                  />
                  <div className='drop-file-preview__item__info'>
                    <p>{item.name}</p>
                    <p>{item.size}B</p>
                  </div>
                  <span
                    className='drop-file-preview__item__del'
                    onClick={() => fileRemove(item)}
                  >
                    x
                  </span>
                </div>
              )
            )
          )}
        </div>
      ) : null}
    </>
  );
};
// DropFileInput.propTypes = {
//   onFileChange: PropTypes.func.isRequired,
// };

export default DropFileInput;

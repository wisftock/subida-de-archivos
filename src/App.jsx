import './App.css';
import DropFileInput from './components/DropFileInput';

function App() {
  const onFileChange = (files) => {
    console.log(files);
  };
  return (
    <div className='box'>
      <h2 className='header'>Subida de archivos</h2>
      <DropFileInput onFileChange={(files) => onFileChange(files)} />
    </div>
  );
}

export default App;
